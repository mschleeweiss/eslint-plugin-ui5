# eslint-plugin-ui5

ESLint rules for UI5 development

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-ui5`:

```
$ npm install eslint-plugin-ui5 --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-ui5` globally.

## Usage

Add `ui5` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "ui5"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "ui5/rule-name": 2
    }
}
```

## Supported Rules

* ⭐ - recommended rules
* ✒️ - fixable rules

### Possible Errors

| | Rule ID | Description |
|-|-|-|
| ⭐ | [ui5/no-global-id](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/blob/master/docs/rules/no-global-id.md) | Prevents accessing controls using `sap.ui.getCore().byId()` |

### Best Practices

| | Rule ID | Description |
|-|-|-|
| ⭐ | [ui5/no-global-name](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/blob/master/docs/rules/no-global-name.md) | Prevents accessing modules in a synchronous way using their global name. |
| ⭐ | [ui5/no-for-in](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/blob/master/docs/rules/no-for-in.md) | Prevents using `for...in` loops. |
| ⭐ | [ui5/no-boolean-literal-compare](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/blob/master/docs/rules/no-boolean-literal-compare.md) | Prevents using boolean literals in a comparison |

### Stylistic Issues

| | Rule ID | Description |
|-|-|-|
| | [ui5/hungarian-notation](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/blob/master/docs/rules/hungarian-notation.md) | Enforces hungarian notation when declaring variables. |






